# XCache

XCache v.2.2 (26/Jul/2013)

XCache can use apc / file / memcache and apc drivers to cache any object. 
All objects can be grouped under same TTL (time to live), to expire in same ammount of seconds.

Can save and retrieve any object / method result / sql result / string / array / file / html

Drivers for: files / apc / memcache / mongodb / xcache 


## Use

1) Edit xcacheconf.php and set your prefered values.

2) Include XCache.php in your code

		require ('XCache.php');

		
3) Instance XCache

		$XCache = XCache::getXCInstance();
		
		
4) Create a group in xcacheconf.php and set TTL.

		$config['cache_test'] = array ('MyTestGroup' => 30);
				
				
5) Call XCache:

		return $XCache->cache('cache_test','MyTestGroup','myUniqueID','MyValueOrObject');
		
		
5) Call a method with XCache :

		$XCache = XCache::getXCInstance();
		return $XCache->cache('cache_test','MyTestGroup','GetProduct_[ProductID]','MyProducts','getProduct','[ProductID]');
		
	This call execute: MyProducts->getProduct($ID)
	Remember to load (include/require) MyProducts class before to xcache call.
		
		
		
Execute 'demo.php' and take a look at the code to see how it works.