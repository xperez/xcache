<?php

/**
 * Enable or disable cache
 */
$config['cache_enabled'] = true;

/**
 * Cache type:  file / apc / memcache / mongodb / xcache
 */
$config['cache_type'] = 'file';

/**
 * Compress contents, used only for file cache
 */
$config['cache_compress'] = true; // true / false

/**
 * Cache specific parameters for memcache or mongodb
 * 
 * Memcache format :   host:post
 * MongoDB format :    host:port:user:password:db:collection
 */
$config['cache_memcache'] = '127.0.0.1:11211'; 

$config['cache_mongodb'] =  '127.0.0.1:27017:::xcachedb:xcachecollection'; 

/**
 * Cache path to save files, used only for file cache
 */
$config['cache_path'] = __DIR__.'/cachefiles/';


/**
 * Cache GET requests
 */
$config['cache_get'] = true;

/**
 * Cache POST requests
 */
$config['cache_get'] = true;

/**
 * Cache control for logged and get parameters
 * 
 * If you use a cookie when user is logged, and wnat to disable cache for logged users
 * Specify the cookie name when logged
 */

$config['cache_logged_cookie'] = 'isloggedcookie';
$config['cache_only_not_logged_pages'] = false;

/*
 * Example cache group type.
 * 
 * Each type can have any 'name' values with it's own expiration time (in seconds) 
 * 
 * Set default to '0' if cache must not be done for not defined 'name'.
 */
$config['cache_test'] = array (
                'default'           => 10, 
                'MyTestValue'       => 10,
                'MyTestCount'       => 3600,
                'MyTestInfo'        => 30,
                'MyObject'          => 5,
                'MyExternallClassCall' => 60
);
